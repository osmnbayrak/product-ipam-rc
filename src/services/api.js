import { stringify } from 'qs';
import request from '@/utils/request';
import axios from 'axios';
import { message } from 'antd';
import { getLocale } from 'umi/locale';

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';
axios.defaults.headers['Content-Type'] = 'application/json';

axios.interceptors.request.use((config) => {
  const locale = getLocale();
  if (locale) {
    config.headers.common['Accept-Language'] = locale;
  }
  return config
});

axios.interceptors.response.use((resp) => {
  return resp;
}, (error => {
  if (error.response.status === 403) {
    window.location.href = '/user/login';
  }
  error.response.data.errors.forEach((err) => {
    message.error(err.detail);
  });
}));

export async function login(params) {
  return axios.post('/api/auth/login/', params).then((response) => {
    return response
  })
}

export async function logout() {
  return axios.post('/api/auth/logout/').then((response) => {
    return response
  }).catch((err) => {
    return err;
  });
}

export async function getCurrentUser() {
  return axios.get(`/api/auth/user/`).then(response => response.data);
}

export async function getMeteringPlaces(params) {
  return axios.get('/api/lott/consumption-place/', { params }).then(response => response.data);
}

export async function getMeteringPlace(params) {
  const { id } = params;
  return axios.get(`/api/lott/consumption-place/${id}/`).then(response => response.data);
}

export async function createMeteringPlace(params) {
  return axios.post('/api/lott/consumption-place/', params).then((response) => {
    return response.data
  }).catch((err) => {
    return err
  });
}

export async function updateMeteringPlace(params) {
  return axios.put(`/api/lott/consumption-place/${id}/`, params).then(response => response.data);
}

export async function deleteMeteringPlace(params) {
  const { id } = params;
  return axios.delete(`/api/lott/consumption-place/${id}/`).then(response => {
    message.success('Başarıyla sildim hacı dayı.');
    return response.data
  });
}

export async function getMeteringDevices(params) {
  return axios.get('/api/lott/meter-device/', { params }).then(response => response.data);
}

export async function getMeteringDevice(params) {
  const { id } = params;
  return axios.get(`/api/lott/meter-device/${id}/`).then(response => response.data);
}

export async function createMeteringDevice(params) {
  return axios.post('/api/lott/meter-device/', params).then((response) => {
    return response.data
  }).catch((err) => {
    return err
  });
}

export async function updateMeteringDevice(params) {
  return axios.put(`/api/lott/meter-device/${id}/`, params).then(response => response.data);
}

export async function deleteMeteringDevice(params) {
  const { id } = params;
  return axios.delete(`/api/lott/meter-device/${id}/`).then(response => {
    message.success('Başarıyla sildim hacı dayı.');
    return response.data
  });
}

export async function getPlannings(params) {
  return axios.get('/api/fobi-form-entry/', { params }).then(response => response.data);
}

export async function planningData(params) {
  return axios.get('/api/form-data/', { params }).then(response => response.data);
}

export async function getSensors(params) {
  return axios.get('/api/tenant/tenants/', { params }).then(response => response.data);
}

export async function getSensor(params) {
  const { id } = params;
  return axios.get(`/api/tenant/tenants/${id}/`).then(response => response.data);
}

export async function createSensor(params) {
  return axios.post('/api/tenant/tenants/', params).then((response) => {
    return response.data
  }).catch((err) => {
    return err
  });
}

export async function updateSensor(params) {
  return axios.put(`/api/nms/nodes/${id}/`, params).then(response => response.data);
}

export async function deleteSensor(params) {
  const { id } = params;
  return axios.delete(`/api/nms/nodes/${id}/`).then(response => {
    message.success('Başarıyla sildim hacı dayı.');
    return response.data
  });
}

export async function getTenants(params) {
  return axios.get('/api/tenant/tenants/', { params }).then(response => response.data);
}

// =======================================================

export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'update',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('/api/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  return request('/api/fake_chart_data');
}

export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile() {
  return request('/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/api/fake_list?${stringify(params)}`);
}

export async function removeFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'delete',
    },
  });
}

export async function addFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'post',
    },
  });
}

export async function updateFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'update',
    },
  });
}

export async function fakeAccountLogin(params) {
  return request('/api/login/account', {
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/api/notices');
}

export async function getFakeCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}
