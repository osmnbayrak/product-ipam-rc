import React, { PureComponent } from 'react';
import { AutoComplete, Input } from 'antd';
import Lexer from './lexer'
import DjangoQL from './completion'

import './completion.css';
import { supportsGoWithoutReloadUsingHash } from 'history/DOMUtils';

const {TextArea} = Input;
const {Option} = AutoComplete;

const reIntValue = '(-?0|-?[1-9][0-9]*)';
const reFractionPart = '\\.[0-9]+';
const reExponentPart = '[eE][+-]?[0-9]+';
const intRegex = new RegExp(reIntValue);
const floatRegex = new RegExp(
  `${reIntValue + reFractionPart + reExponentPart  }|${ 
  reIntValue  }${reFractionPart  }|${ 
  reIntValue  }${reExponentPart}`);
const reLineTerminators = '\\n\\r\\u2028\\u2029';
const reEscapedChar = '\\\\[\\\\"/bfnrt]';
const reEscapedUnicode = '\\\\u[0-9A-Fa-f]{4}';
const reStringChar = `[^\\"\\\\${  reLineTerminators  }]`;
const stringRegex = new RegExp(
  `\\"(${  reEscapedChar 
  }|${  reEscapedUnicode 
  }|${  reStringChar  })*\\"`);
const nameRegex = /[_A-Za-z][_0-9A-Za-z]*(\.[_A-Za-z][_0-9A-Za-z]*)*/;
const reNotFollowedByName = '(?![_0-9A-Za-z])';
const whitespaceRegex = /[ \t\v\f\u00A0]+/;


function token(name, value) {
  return { name, value };
}

const lexer = new Lexer(() => {
  // Silently swallow any lexer errors
});

function suggestion(text, snippetBefore, snippetAfter) {
  // text is being displayed in completion box and pasted when you hit Enter.
  // snippetBefore is an optional extra text to be pasted before main text.
  // snippetAfter is an optional text to be pasted after. It may also include
  // "|" symbol to designate desired cursor position after paste.
  return {
    text,
    snippetBefore: snippetBefore || '',
    snippetAfter: snippetAfter || ''
  };
}


export default class InputQuery extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentText: '',
      currentModel: null,
      models: {},
      token,
      lexer,
      prefix: '',
      suggestions: [],
      tmpSuggestions: [],
      selected: null,
      valuesCaseSensitive: false,
      highlightCaseSensitive: true,
      completion: null,
      completionOptions: null,
      completionEnabled: true,
    };

    lexer.addRule(whitespaceRegex, () => { /* ignore whitespace */ });
    lexer.addRule(/\./, (l) => token('DOT', l));
    lexer.addRule(/,/, (l) => token('COMMA', l));
    lexer.addRule(new RegExp(`or${  reNotFollowedByName}`), (l) => token('OR', l));
    lexer.addRule(new RegExp(`and${  reNotFollowedByName}`), (l)  => token('AND', l));
    lexer.addRule(new RegExp(`not${  reNotFollowedByName}`), (l) => token('NOT', l));
    lexer.addRule(new RegExp(`in${  reNotFollowedByName}`), (l) => token('IN', l));
    lexer.addRule(new RegExp(`True${  reNotFollowedByName}`), (l) => token('TRUE', l));
    lexer.addRule(new RegExp(`False${  reNotFollowedByName}`), (l) => token('FALSE', l));
    lexer.addRule(new RegExp(`None${  reNotFollowedByName}`), (l) => token('NONE', l));
    lexer.addRule(nameRegex, (l) => token('NAME', l));
    lexer.addRule(stringRegex, (l) =>
      // Trim leading and trailing quotes
      token('STRING_VALUE', l.slice(1, l.length - 1))
    );
    lexer.addRule(intRegex, (l) => token('INT_VALUE', l));
    lexer.addRule(floatRegex, (l) => token('FLOAT_VALUE', l));
    lexer.addRule(/\(/, (l) => token('PAREN_L', l));
    lexer.addRule(/\)/, (l) => token('PAREN_R', l));
    lexer.addRule(/=/, (l) => token('EQUALS', l));
    lexer.addRule(/!=/, (l) => token('NOT_EQUALS', l));
    lexer.addRule(/>/, (l) => token('GREATER', l));
    lexer.addRule(/>=/, (l) => token('GREATER_EQUAL', l));
    lexer.addRule(/</, (l) => token('LESS', l));
    lexer.addRule(/<=/, (l) => token('LESS_EQUAL', l));
    lexer.addRule(/~/, (l) => token('CONTAINS', l));
    lexer.addRule(/!~/, (l) => token('NOT_CONTAINS', l));
    lexer.lexAll = () => {
      let match;
      const result = [];
      // console.log('what the hell');
      while (match = lexer.lex()) {  // eslint-disable-line no-cond-assign
        match.start = lexer.index - match.value.length;
        match.end = lexer.index;
        result.push(match);
      }
      return result;
    };

    this.textareaRef = React.createRef();
    this.autoCompleteRef = React.createRef();

    this.loadIntrospections(props.options.introspections)
  }

  onCompletionMouseClick = (e) => {
    // console.log('this is e', e);
    // e.value = 'Daha dun annemizin';
    this.selectCompletion(parseInt(e.item.props.index, 10), e);
    // this.generateSuggestions(this.textareaRef);
  };

  onCompletionMouseDown = (e) => {
    // This is needed to prevent 'blur' event on textarea
    e.preventDefault();
  };

  onCompletionMouseOut = () => {
    this.setState({
      selected: null
    });
    // this.debouncedRenderCompletion();
  };

  onCompletionMouseOver = (e) => {
    this.setState({
      selected: parseInt(e.item.props.index, 10)
    });
    // this.debouncedRenderCompletion();
  };

  onKeyDown = (e) => {
    const {suggestions, selected} = this.state;
    switch (e.keyCode) {
      case 38:  // up arrow
        if (suggestions.length) {
          if (selected === null) {
            this.setState({selected: suggestions.length - 1});
          } else if (selected === 0) {
            this.setState({selected: null});
          } else {
            this.setState({selected: selected - 1});
          }
          // this.renderCompletion();
          e.preventDefault();
        }
        break;

      case 40:  // down arrow
        if (suggestions.length) {
          if (selected === null) {
            this.setState({selected: 0});
          } else if (selected < suggestions.length - 1) {
            this.setState({selected: selected + 1});
          } else {
            this.setState({selected: null});
          }
          // this.renderCompletion();
          e.preventDefault();
        }
        break;

      case 9:   // Tab
        if (selected !== null) {
          this.selectCompletion(selected);
          e.preventDefault();
        }
        break;

      case 13:  // Enter
        if (selected !== null) {
          this.selectCompletion(selected);
        }
        // e.preventDefault();
        break;

      case 27:  // Esc
        this.hideCompletion();
        break;

      case 16:  // Shift
      case 17:  // Ctrl
      case 18:  // Alt
      case 91:  // Windows Key or Left Cmd on Mac
      case 93:  // Windows Menu or Right Cmd on Mac
        // Control keys shouldn't trigger completion popup
        break;

      default:
        // When keydown is fired input value has not been updated yet,
        // so we need to wait
        window.setTimeout(this.popupCompletion, 10);
        break;
    }
  };

  selectCompletion = (index) => {
    const {
      prefix,
      suggestions
    } = this.state;
    const startPos = this.textareaRef.input.selectionStart - prefix.length;
    const textAfter = this.textareaRef.input.value.slice(startPos + prefix.length);
    const textBefore = this.textareaRef.input.value.slice(0, startPos);

    const snippetAfterParts = suggestions[index].snippetAfter.split('|');
    const textToPaste = suggestions[index].snippetBefore +
      suggestions[index].text +
      snippetAfterParts.join('');
    let cursorPosAfter = textBefore.length + textToPaste.length;
    if (snippetAfterParts.length > 1) {
      cursorPosAfter -= snippetAfterParts[1].length;
    }

    // console.log('Last debugging I hope textBefore', textBefore);
    // console.log('Last debugging I hope textToPaste', textToPaste);
    // console.log('Last debugging I hope textAfter', textAfter);
    // console.log('bu nedir babaaaa', textBefore + textToPaste + textAfter);
    // this.autoCompleteRef.value = "Ali ata bak.";
    // console.log('autoCompleteRef ', this.autoCompleteRef);
    // this.textareaRef.value = "Ali ata bak.";
    this.textareaRef.input.focus();
    this.textareaRef.input.setSelectionRange(cursorPosAfter, cursorPosAfter);
    // this.setState({
    //   currentText: textBefore + textToPaste + textAfter,
    //   selected: null
    // });
    // if (this.textareaResize) {
    //   this.textareaResize();
    // }
    this.generateSuggestions(this.textareaRef);
    // this.renderCompletion();
  };

  selectCompletionText = (index) => {
    const {
      prefix,
      suggestions
    } = this.state;
    const startPos = this.textareaRef.input.selectionStart - prefix.length;
    const textAfter = this.textareaRef.input.value.slice(startPos + prefix.length);
    const textBefore = this.textareaRef.input.value.slice(0, startPos);

    const snippetAfterParts = suggestions[index].snippetAfter.split('|');
    const textToPaste = suggestions[index].snippetBefore +
      suggestions[index].text +
      snippetAfterParts.join('');
    let cursorPosAfter = textBefore.length + textToPaste.length;
    if (snippetAfterParts.length > 1) {
      cursorPosAfter -= snippetAfterParts[1].length;
    }

    // console.log('Last debugging I hope textBefore', textBefore);
    // console.log('Last debugging I hope textToPaste', textToPaste);
    // console.log('Last debugging I hope textAfter', textAfter);
    // console.log('bu nedir babaaaa', textBefore + textToPaste + textAfter);
    // this.autoCompleteRef.value = "Ali ata bak.";
    // console.log('autoCompleteRef ', this.autoCompleteRef);
    // this.textareaRef.value = "Ali ata bak.";
    this.textareaRef.input.setSelectionRange(cursorPosAfter, cursorPosAfter);
    // this.setState({
    //   currentText: textBefore + textToPaste + textAfter,
    //   selected: null
    // });
    // if (this.textareaResize) {
    //   this.textareaResize();
    // }
    // this.renderCompletion();
    return textBefore + textToPaste + textAfter;

  };

  popupCompletion = () => {
    this.generateSuggestions();
    // this.renderCompletion();
  };

  generateSuggestions = () => {
    const {
      models,
      completionEnabled,
      prefix,
      suggestions,
      currentModel,
      valuesCaseSensitive
    } = this.state;
    const input = this.textareaRef.input;
    let tmpSuggestions;
    let snippetBefore;
    let snippetAfter;
    let searchFilter;
    let generatedSuggestions = suggestions;
    let localPrefix = prefix;

    if (!completionEnabled) {
      localPrefix = '';
      generatedSuggestions = [];
      this.setState({
        suggestions: generatedSuggestions,
        prefix: localPrefix
      });
      return;
    }

    if (!currentModel) {
      // Introspections are not loaded yet
      return;
    }
    if (input.selectionStart !== input.selectionEnd) {
      // We shouldn't show tmpSuggestions when something is selected
      localPrefix = '';
      generatedSuggestions = [];
      this.setState({
        suggestions: generatedSuggestions,
        prefix: localPrefix
      });
      return;
    }

    // default search filter - find anywhere in the string, case-sensitive
    searchFilter = (item) => {
      // console.log('Number one haci');
      return item.text.indexOf(prefix) >= 0;
    };

    // console.log('this is the input values ', input);

    const context = this.getContext(input.value, input.selectionStart);
    localPrefix = context.prefix;
    const model = models[context.model];
    const field = context.field && model[context.field];

    const textBefore = input.value.slice(0, input.selectionStart - localPrefix.length);
    const textAfter = input.value.slice(input.selectionStart);

    // console.log('Our context is this ', context);

    switch (context.scope) {
      case 'field':
        // console.log('In Field');
        generatedSuggestions = Object.keys(model).map((f) =>
          suggestion(f, '', model[f].type === 'relation' ? '.' : ' ')
        );
        // console.log('What is going on here ? ', suggestions);
        break;

      case 'comparison':
        tmpSuggestions = ['=', '!='];
        snippetAfter = ' ';
        if (field && field.type !== 'bool') {
          if (field.type === 'str') {
            tmpSuggestions.push('~');
            tmpSuggestions.push('!~');
            snippetAfter = ' "|"';
          } else if (field.type === 'date' || field.type === 'datetime') {
            snippetAfter = ' "|"';
          }
          Array.prototype.push.apply(tmpSuggestions, ['>', '>=', '<', '<=']);
        }

        generatedSuggestions = tmpSuggestions.map((s) =>
          suggestion(s, '', snippetAfter)
        );

        if (field && field.type !== 'bool') {
          if (['str', 'date', 'datetime'].indexOf(field.type) >= 0) {
            snippetAfter = ' ("|")';
          } else {
            snippetAfter = ' (|)';
          }
          generatedSuggestions.push(suggestion('in', '', snippetAfter));
          generatedSuggestions.push(suggestion('not in', '', snippetAfter));
        }
        // use "starts with" search filter instead of default
        searchFilter = (item) => {
          // console.log('Number two haci');
          // See http://stackoverflow.com/a/4579228
          return item.text.lastIndexOf(localPrefix, 0) === 0;
        };
        break;

      case 'value':
        if (!field) {
          // related field
          generatedSuggestions = [suggestion('None', '', ' ')];
        } else if (field.type === 'str') {
          if (textBefore && textBefore[textBefore.length - 1] === '"') {
            snippetBefore = '';
          } else {
            snippetBefore = '"';
          }
          if (textAfter[0] !== '"') {
            snippetAfter = '" ';
          } else {
            snippetAfter = '';
          }
          if (!valuesCaseSensitive) {
            searchFilter = (item) => {
              // Case-insensitive
              // console.log('Number three haci');
              return item.text.toLowerCase().indexOf(localPrefix.toLowerCase()) >= 0;
            }
          }
          generatedSuggestions = field.options.map((f) =>
            suggestion(f, snippetBefore, snippetAfter)
          );
        } else if (field.type === 'bool') {
          generatedSuggestions = [
            suggestion('True', '', ' '),
            suggestion('False', '', ' ')
          ];
          if (field.nullable) {
            generatedSuggestions.push(suggestion('None', '', ' '));
          }
        } else if (field.type === 'unknown') {
          // unknown field type, reset tmpSuggestions
          generatedSuggestions = [];
          localPrefix = ''
        }
        break;

      case 'logical':
        generatedSuggestions = [
          suggestion('and', '', ' '),
          suggestion('or', '', ' ')
        ];
        break;

      default:
        generatedSuggestions = [];
        localPrefix = '';
    }
    generatedSuggestions = generatedSuggestions.filter(searchFilter);
    // console.log('searchFilter', searchFilter);
    // console.log('Suggestionlar null olamaz! ', generatedSuggestions);

    this.setState({
      suggestions: generatedSuggestions,
      prefix: localPrefix
    });

    if (generatedSuggestions.length === 1) {
      this.setState({selected: 0});  // auto-select the only suggested item
    } else {
      this.setState({selected: null});
    }
  };

  hideCompletion = () => {
    const { completion } = this.state;
    this.setState({selected: null});
    // if (completion) {
    //
    //   this.completion.style.display = 'none';
    // }
  };

  loadIntrospections = (introspections) => {
    let onLoadError;
    let request;
    if (typeof introspections === 'string') {
      // treat as URL
      onLoadError = () => {
        this.logError(`failed to load introspections from ${  introspections}`);
      };
      request = new XMLHttpRequest();
      request.open('GET', introspections, true);
      request.onload = () => {
        let data;
        if (request.status === 200) {
          data = JSON.parse(request.responseText);
          this.setState({
            currentModel: data.current_model,
            models: data.models
          });
        } else {
          onLoadError();
        }
      };
      request.ontimeout = onLoadError;
      request.onerror = onLoadError;
      /* eslint-disable max-len */
      // Workaround for IE9, see
      // https://cypressnorth.com/programming/internet-explorer-aborting-ajax-requests-fixed/
      /* eslint-enable max-len */
      request.onprogress = () => {};
      window.setTimeout(request.send.bind(request));
    } else if (this.isObject(introspections)) {
      this.setState({
        currentModel: introspections.current_model,
        models: introspections.models
      });
    } else {
      this.logError(
        `${'introspections parameter is expected to be either URL or ' +
        'object with definitions, but '}${  introspections  } was found`);
    }
  };

  getContext = (text, cursorPos) => {
    const {
      lexer,
      currentModel,
      models
    } = this.state;
    // This function returns an object with the following 4 properties:
    let prefix;        // text already entered by user in the current scope
    let scope = null;  // 'field', 'comparison', 'value', 'logical' or null
    let model = null;  // model, set for 'field', 'comparison' and 'value'
    let field = null;  // field, set for 'comparison' and 'value'

    let nameParts;
    let resolvedName;
    let lastToken = null;
    let nextToLastToken = null;
    const tokens = lexer.setInput(text.slice(0, cursorPos)).lexAll();
    // console.log('These are tokens', tokens);
    // console.log('This is text', text);
    if (tokens.length && tokens[tokens.length - 1].end >= cursorPos) {
      // if cursor is positioned on the last token then remove it.
      // We are only interested in tokens preceding current.
      tokens.pop();
    }
    if (tokens.length) {
      lastToken = tokens[tokens.length - 1];
      if (tokens.length > 1) {
        nextToLastToken = tokens[tokens.length - 2];
      }
    }

    // Current token which is currently being typed may be not complete yet,
    // so lexer may fail to recognize it correctly. So we define current token
    // prefix as a string without whitespace positioned after previous token
    // and until current cursor position.
    prefix = text.slice(lastToken ? lastToken.end : 0, cursorPos);
    const whitespace = prefix.match(whitespaceRegex);
    if (whitespace) {
      prefix = prefix.slice(whitespace[0].length);
    }
    if (prefix === '(') {
      // Paren should not be a part of suggestion
      prefix = '';
    }

    // console.log('lastToken ', lastToken);
    // console.log('whitespace ', whitespace);
    // console.log('nextToLastToken ', nextToLastToken);
    // console.log('prefix ', prefix);
    // console.log('lastToken ', lastToken);
    // console.log('lastToken ', lastToken);

    if (prefix === ')' && !whitespace) {
      // Nothing to suggest right after right paren
    } else if (!lastToken ||
      (['AND', 'OR'].indexOf(lastToken.name) >= 0 && whitespace) ||
      (prefix === '.' && lastToken && !whitespace) ||
      (lastToken.name === 'PAREN_L' && (!nextToLastToken ||
        ['AND', 'OR'].indexOf(nextToLastToken.name) >= 0))) {
      // console.log('Girdimm');
      scope = 'field';
      model = currentModel;
      if (prefix === '.') {
        prefix = text.slice(lastToken.start, cursorPos);
      }
      nameParts = prefix.split('.');
      if (nameParts.length > 1) {
        // use last part as a prefix, analyze preceding parts to get the model
        prefix = nameParts.pop();
        resolvedName = this.resolveName(nameParts.join('.'));
        if (resolvedName.model && !resolvedName.field) {
          model = resolvedName.model;
        } else {
          // if resolvedName.model is null that means that model wasn't found.
          // if resolvedName.field is NOT null that means that the name
          // preceding current prefix is a concrete field and not a relation,
          // and therefore it can't have any properties.
          scope = null;
          model = null;
        }
      }
    } else if (lastToken && whitespace &&
      nextToLastToken && nextToLastToken.name === 'NAME' &&
      ['EQUALS', 'NOT_EQUALS', 'CONTAINS', 'NOT_CONTAINS', 'GREATER_EQUAL',
        'GREATER', 'LESS_EQUAL', 'LESS'].indexOf(lastToken.name) >= 0) {
      resolvedName = this.resolveName(nextToLastToken.value);
      if (resolvedName.model) {
        scope = 'value';
        model = resolvedName.model;
        field = resolvedName.field;
        if (prefix[0] === '"' && models[model][field].type === 'str') {
          prefix = prefix.slice(1);
        }
      }
    } else if (lastToken && whitespace && lastToken.name === 'NAME') {
      resolvedName = this.resolveName(lastToken.value);
      if (resolvedName.model) {
        scope = 'comparison';
        model = resolvedName.model;
        field = resolvedName.field;
      }
    } else if (lastToken && whitespace &&
      ['PAREN_R', 'INT_VALUE', 'FLOAT_VALUE', 'STRING_VALUE']
        .indexOf(lastToken.name) >= 0) {
      scope = 'logical';
    }
    // console.log('At the end of getContext ', prefix, scope, model, field);
    return { prefix, scope, model, field };
  };

  resolveName = (name) => {
    const {
      models,
      currentModel
    } = this.state;

    // Walk through introspection definitions and get target model and field
    let f;
    let i;
    let l;
    const nameParts = name.split('.');
    let model = currentModel;
    let field = null;

    // console.log('CurrentModel ', currentModel);
    // console.log('nameeeeeeeeee ', name);
    // console.log('CurrentModel ', currentModel);

    if (model) {
      for (i = 0, l = nameParts.length; i < l; i++) {
        f = models[model][nameParts[i]];
        if (!f) {
          model = null;
          field = null;
          break;
        } else if (f.type === 'relation') {
          model = f.relation;
          field = null;
        } else {
          field = nameParts[i];
        }
      }
    }
    return { model, field };
  };

  onInputChange = (e) => {
    console.log('Testimsi', e)
  };

  onCompleteChange = (e) => {
    console.log('Testimsi2', e)
  };

  onOptionChange = (e) => {
    console.log('Testimsi3', e)
  };

  render() {
    // const testingThisOne = new DjangoQL({
    //   // either JS object with a result of DjangoQLSchema(MyModel).as_dict(),
    //   // or an URL from which this information could be loaded asynchronously
    //   introspections: 'http://localhost:8000/api/lott/consumption-place/introspect/',
    //
    //   // css selector for query input or HTMLElement object.
    //   // It should be a textarea
    //   selector: 'textarea[name=q]',
    //   syntaxHelp: null,
    //   autoResize: true
    // });
    // console.log(testingThisOne);
    const {suggestions, currentText} = this.state;
    console.log('currentText ', currentText);

    const completionOptions = suggestions.map((item, index) =>
      <Option
        key={item.text}
        text={this.selectCompletionText(index)}
        onClick={(e) => this.onCompletionMouseClick(e)}
        onChange={(e) => this.onOptionChange(e)}
        onMouseDown={(e) => this.onCompletionMouseDown(e)}
        onBlur={() => this.onCompletionMouseOut()}
        onFocus={(e) => this.onCompletionMouseOver(e)}
      >
        {item.text}
      </Option>
    );

    return (
      <AutoComplete
        dataSource={completionOptions}
        dropdownStyle={{width: '300px'}}
        optionLabelProp='text'
        onChange={(e) => this.onCompleteChange(e)}
        ref={ref => {this.autoCompleteRef = ref}}
      >
        <Input
          // value={currentText}
          name='q'
          onKeyDown={(e) => this.onKeyDown(e)}
          onBlur={(e) => this.hideCompletion(e)}
          onClick={() => this.popupCompletion()}
          onChange={(e) => this.onInputChange(e)}
          ref={ref => {this.textareaRef = ref}}
          autocomplete='off'
          autosize
        />
      </AutoComplete>
    )
  }
}
