import React, { Fragment } from 'react';
import { Layout, Icon } from 'antd';
import GlobalFooter from '@/components/GlobalFooter';

const { Footer } = Layout;
const FooterView = () => (
  <Footer style={{ padding: 0 }}>
    <GlobalFooter
      links={[
        {
          key: 'Ant Design',
          title: 'Ant Design',
          href: 'https://ant.design',
          blankTarget: true,
        },
      ]}
      copyright={
        <Fragment>
          <a target="_blank" href="//www.sekomyazilim.com.tr">Copyright <Icon type="copyright" /> Sekom Yazilim</a>
        </Fragment>
      }
    />
  </Footer>
);
export default FooterView;
