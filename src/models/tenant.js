import {
  getTenants
} from '@/services/api';

export default {
  namespace: 'tenant',

  state: {
    results: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(getTenants, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        results: action.payload,
      };
    },
  },
};
