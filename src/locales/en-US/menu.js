export default {
  'menu.dashboard': 'Dashboard',
  'menu.metering-places': 'Metering Places',
  'menu.metering-devices': 'Metering Devices',
  'menu.VRF': 'VRF',
  'menu.sensor-messages': 'Sensor Messages',
  'menu.installations': 'Installations',
  'menu.notifications': 'Notifications',
  'menu.reports': 'Reports',
  'menu.Planning': 'Planning'
};
