import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  Modal,
  message,
  Divider, Icon, Menu, Dropdown, Select, InputNumber, DatePicker,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './MeteringDevices.less';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title={<FormattedMessage id="title.place.floors.create" defaultMessage="Create Floor" />}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.name" defaultMessage="Floor Name" />}>
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: 'Bu alan zorunludur.' }],
        })(<Input placeholder={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Floor Name" })} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.level" defaultMessage="Floor Level" />}>
        {form.getFieldDecorator('level', {
          rules: [{ required: true, message: 'Bu alan zorunludur.' }],
        })(<Input placeholder={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Floor Level" })} />)}
      </FormItem>
    </Modal>
  );
});

@Form.create()
class UpdateForm extends PureComponent {
  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  render() {
    const {
      form,
      handleUpdate,
      values
    } = this.props;

    const { updateModalVisible, handleUpdateModalVisible } = this.props;
    const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(values.id, fieldsValue);
      });
    };
    return (
      <Modal
        width={640}
        bodyStyle={{ padding: '32px 40px 48px' }}
        destroyOnClose
        title={formatMessage({ id: 'title.update', defaultMessage:"Update Form" })}
        style={{ width: '100%' }}
        visible={updateModalVisible}
        onCancel={() => handleUpdateModalVisible()}
        onOk={okHandle}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.name" defaultMessage="Floor Name" />}>
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: 'Bu alan zorunludur.' }],
            initialValue: values.attributes.name
          })(<Input placeholder={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Floor Name" })} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.level" defaultMessage="Floor Level" />}>
          {form.getFieldDecorator('level', {
            rules: [{ required: true, message: 'Bu alan zorunludur.' }],
            initialValue: values.attributes.level
          })(<Input placeholder={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Floor Level" })} />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ meteringDevices, tenant, loading }) => ({
  meteringDevices,
  tenant,
  loading: loading.models.meteringDevices && loading.models.tenant,
}))
@Form.create()
class TableList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    pagination: {
      page_size: 10,
      page: 1
    }
  };

  columns = [
    {
      title: <FormattedMessage id="title.place.floors.level" defaultMessage="Tenant" />,
      dataIndex: 'tenant.name',
    },
    {
      title: <FormattedMessage id="title.place.floors.name" defaultMessage="Metering Device ID" />,
      dataIndex: 'name',
    },
    {
      title: <FormattedMessage id="title.place.floors.name" defaultMessage="Consumption Place" />,
      dataIndex: 'consumption_place.evidence_consumption_place_id',
    },
    {
      title: <FormattedMessage id="title.place.floors.level" defaultMessage="# Connected Sensors" />,
      render: (text, record) => (
        record.sensors.length
      ),
    },
    {
      title: <FormattedMessage id="title.actions" defaultMessage="Actions" />,
      align: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>
            <FormattedMessage id="btn.edit" defaultMessage="Edit" />
          </a>
          <Divider type="vertical" />
          <a onClick={() => this.handleRemove(record)}>
            <FormattedMessage id="btn.delete" defaultMessage="Delete" />
          </a>
          <Divider type="vertical" />
          <a onClick={() => this.handleRemove(record)}>
            <FormattedMessage id="btn.detail" defaultMessage="Detail" />
          </a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch({
      type: 'meteringDevices/fetch',
      payload: this.state.pagination,
    });

    dispatch({
      type: 'tenant/fetch',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'meteringDevices/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'meteringDevices/fetch',
      payload: {},
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'meteringDevices/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      stepFormValues: record || {},
    });
  };

  handleRemove = (record) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'meteringDevices/delete',
      payload: {
        id: record.id,
      },
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'meteringDevices/create',
      payload: {
        name: fields.name,
        level: fields.level,
      },
    }).then(() => {
      dispatch({
        type: 'meteringDevices/fetch',
      });
      this.handleModalVisible();
    }).catch(() => {
      dispatch({
        type: 'meteringDevices/fetch',
      });
    });

    // this.handleModalVisible();
  };

  handleUpdate = (id, fields) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'meteringDevices/update',
      payload: {
        id,
        name: fields.name,
        level: fields.level,
      },
    });

    message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
      tenant
    } = this.props;

    console.log('this is tenant', tenant);

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })}>
              {getFieldDecorator('name__icontains')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })}>
              {getFieldDecorator('consumption_place')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })}>
              {getFieldDecorator('sensors')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })} />)}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              Clear
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              Expand <Icon type="down" />
            </a>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              Advanced
            </a>
          </div>
        </div>
      </Form>
    );
  }

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
      tenant
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })}>
              {getFieldDecorator('name__icontains')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })}>
              {getFieldDecorator('consumption_place')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })}>
              {getFieldDecorator('sensors')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })} />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="Tenant">
              {getFieldDecorator('tenant')(
                <Select placeholder="Tenant" style={{ width: '100%' }}>
                  {tenant.results.map((item) =>
                    <Option value={item.id}>{item.name}</Option>
                  )}
                </Select>
              )}
            </FormItem>
          </Col>
          {/*<Col md={8} sm={24}>*/}
            {/*<FormItem label="使用状态">*/}
              {/*{getFieldDecorator('status3')(*/}
                {/*<Select placeholder="请选择" style={{ width: '100%' }}>*/}
                  {/*<Option value="0">关闭</Option>*/}
                  {/*<Option value="1">运行中</Option>*/}
                {/*</Select>*/}
              {/*)}*/}
            {/*</FormItem>*/}
          {/*</Col>*/}
          {/*<Col md={8} sm={24}>*/}
            {/*<FormItem label="使用状态">*/}
              {/*{getFieldDecorator('status4')(*/}
                {/*<Select placeholder="请选择" style={{ width: '100%' }}>*/}
                  {/*<Option value="0">关闭</Option>*/}
                  {/*<Option value="1">运行中</Option>*/}
                {/*</Select>*/}
              {/*)}*/}
            {/*</FormItem>*/}
          {/*</Col>*/}
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              Clear
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              Collapse <Icon type="up" />
            </a>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              Advanced
            </a>
          </div>
        </div>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    const {
      meteringDevices,
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, stepFormValues } = this.state;
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const menu = (
      <Menu>
        {/*<Menu.Item key="1"><Icon type="plus" />Add Sensor</Menu.Item>*/}
        {/*<Menu.Item key="2"><Icon type="upload" />Import Meter List</Menu.Item>*/}
        <Menu.Item key="3"><Icon type="download" />Export Metering Device List</Menu.Item>
      </Menu>
    );

    const actionList = (
      <Dropdown.Button
        overlay={menu}
        // disabled
        // style={{ marginLeft: 8 }}
      >
        <Icon type="plus"  /> Create
      </Dropdown.Button>
    );

    return (
      <PageHeaderWrapper title='Metering Devices' content={this.renderForm()} action={actionList} count={meteringDevices.count}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            {/*<div className={styles.tableListForm}></div>*/}
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              dataSource={meteringDevices.results}
              // rowKey='id'
              columns={this.columns}
              onChange={this.handleStandardTableChange}
              size='middle'
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        {stepFormValues && Object.keys(stepFormValues).length ? (
          <UpdateForm
            {...updateMethods}
            updateModalVisible={updateModalVisible}
            values={stepFormValues}
          />
        ) : null}
      </PageHeaderWrapper>
    );
  }
}

export default TableList;
