import {
  getMeteringDevices,
  createMeteringDevice,
  updateMeteringDevice,
  deleteMeteringDevice
} from '@/services/api';

export default {
  namespace: 'meteringDevices',

  state: {
    results: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(getMeteringDevices, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *create({ payload }, { call }) {
      yield call(createMeteringDevice, payload);
    },
    *update({ payload }, { call }) {
      yield call(updateMeteringDevice, payload);
    },
    *delete({ payload }, { call }) {
      yield call(deleteMeteringDevice, payload);
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
