import {
  getMeteringPlaces,
  createMeteringPlace,
  updateMeteringPlace,
  deleteMeteringPlace
} from '@/services/api';

export default {
  namespace: 'meteringPlaces',

  state: {
    results: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(getMeteringPlaces, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *create({ payload }, { call }) {
      yield call(createMeteringPlace, payload);
    },
    *update({ payload }, { call }) {
      yield call(updateMeteringPlace, payload);
    },
    *delete({ payload }, { call }) {
      yield call(deleteMeteringPlace, payload);
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
