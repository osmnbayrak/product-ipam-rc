import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  Modal,
  message,
  Divider, Icon, Menu, Dropdown, Select
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './MeteringPlaces.less';
import InputQuery from '../../components/InputQuery';

const { TextArea } = Input;

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title={<FormattedMessage id="title.place.floors.create" defaultMessage="Create Floor" />}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.name" defaultMessage="Floor Name" />}>
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: 'Bu alan zorunludur.' }],
        })(<Input placeholder={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Floor Name" })} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.level" defaultMessage="Floor Level" />}>
        {form.getFieldDecorator('level', {
          rules: [{ required: true, message: 'Bu alan zorunludur.' }],
        })(<Input placeholder={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Floor Level" })} />)}
      </FormItem>
    </Modal>
  );
});

@Form.create()
class UpdateForm extends PureComponent {
  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  render() {
    const {
      form,
      handleUpdate,
      values
    } = this.props;

    const { updateModalVisible, handleUpdateModalVisible } = this.props;
    const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(values.id, fieldsValue);
      });
    };
    return (
      <Modal
        width={640}
        bodyStyle={{ padding: '32px 40px 48px' }}
        destroyOnClose
        title={formatMessage({ id: 'title.update', defaultMessage:"Update Form" })}
        style={{ width: '100%' }}
        visible={updateModalVisible}
        onCancel={() => handleUpdateModalVisible()}
        onOk={okHandle}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.name" defaultMessage="Floor Name" />}>
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: 'Bu alan zorunludur.' }],
            initialValue: values.attributes.name
          })(<Input placeholder={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Floor Name" })} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.level" defaultMessage="Floor Level" />}>
          {form.getFieldDecorator('level', {
            rules: [{ required: true, message: 'Bu alan zorunludur.' }],
            initialValue: values.attributes.level
          })(<Input placeholder={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Floor Level" })} />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ meteringPlaces, tenant, loading }) => ({
  meteringPlaces,
  tenant,
  loading: loading.models.meteringPlaces,
}))
@Form.create()
class TableList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    advancedQueryForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    pagination: {
      page_size: 10,
      page: 1
    }
  };

  columns = [
    {
      title: <FormattedMessage id="title.place.floors.level" defaultMessage="Tenant" />,
      dataIndex: 'tenant.name',
    },
    {
      title: <FormattedMessage id="title.place.floors.name" defaultMessage="Evidence CP ID" />,
      dataIndex: 'evidence_consumption_place_id',
    },
    {
      title: <FormattedMessage id="title.place.floors.level" defaultMessage="Technical CP ID" />,
      dataIndex: 'consumption_place_id',
    },
    {
      title: <FormattedMessage id="title.place.floors.level" defaultMessage="City" />,
      dataIndex: 'city.name',
    },
    {
      title: <FormattedMessage id="title.place.floors.level" defaultMessage="Address" />,
      dataIndex: 'address',
    },
    {
      title: <FormattedMessage id="title.actions" defaultMessage="Actions" />,
      align: 'right',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>
            <FormattedMessage id="btn.edit" defaultMessage="Edit" />
          </a>
          <Divider type="vertical" />
          <a onClick={() => this.handleRemove(record)}>
            <FormattedMessage id="btn.delete" defaultMessage="Delete" />
          </a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch({
      type: 'meteringPlaces/fetch',
      payload: this.state.pagination,
    });

    dispatch({
      type: 'tenant/fetch'
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'meteringPlaces/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    const { pagination } = this.state;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'meteringPlaces/fetch',
      payload: pagination
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;
    const { pagination } = this.state;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'meteringPlaces/fetch',
        payload: {...values, ...pagination}
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      stepFormValues: record || {},
    });
  };

  handleRemove = (record) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'meteringPlaces/delete',
      payload: {
        id: record.id,
      },
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'meteringPlaces/create',
      payload: {
        name: fields.name,
        level: fields.level,
      },
    }).then(() => {
      dispatch({
        type: 'meteringPlaces/fetch',
      });
      this.handleModalVisible();
    }).catch(() => {
      dispatch({
        type: 'meteringPlaces/fetch',
      });
    });

    // this.handleModalVisible();
  };

  handleUpdate = (id, fields) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'meteringPlaces/update',
      payload: {
        id,
        name: fields.name,
        level: fields.level,
      },
    });

    message.success('配置成功');
    this.handleUpdateModalVisible();
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  toggleAdvancedForm = () => {
    const { advancedQueryForm } = this.state;
    this.setState({
      advancedQueryForm: !advancedQueryForm,
    })
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })}>
              {getFieldDecorator('name__icontains')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })}>
              {getFieldDecorator('consumption_place')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })}>
              {getFieldDecorator('sensors')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })} />)}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              Clear
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              Expand <Icon type="down" />
            </a>
            <a style={{ marginLeft: 8 }} onClick={this.toggleAdvancedForm}>
              Advanced
            </a>
          </div>
        </div>
      </Form>
    );
  }

  renderExpandedSimpleForm() {
    const {
      form: { getFieldDecorator },
      tenant
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })}>
              {getFieldDecorator('name__icontains')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.meteringDeviceID', defaultMessage:"Metering Device ID" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })}>
              {getFieldDecorator('consumption_place')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.consumptionPlace', defaultMessage:"Consumption Place" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })}>
              {getFieldDecorator('sensors')(<Input placeholder={formatMessage({ id: 'title.meteringDevices.sensor', defaultMessage:"Sensor" })} />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="Tenant">
              {getFieldDecorator('tenant')(
                <Select placeholder="Tenant" style={{ width: '100%' }} allowClear='true'>
                  {tenant.results.map((item) =>
                    <Option value={item.id}>{item.name}</Option>
                  )}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', marginBottom: 24 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
            <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
              Clear
            </Button>
            <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
              Collapse <Icon type="up" />
            </a>
            <a style={{ marginLeft: 8 }} onClick={this.toggleAdvancedForm}>
              Advanced
            </a>
          </div>
        </div>
      </Form>
    );
  }

  renderAdvancedQueryForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={24} sm={24}>
            <FormItem>
              {getFieldDecorator('q')(<InputQuery
                options={{introspections: 'http://localhost:8000/api/lott/consumption-place/introspect/'}}
              />)}
            </FormItem>
          </Col>
        </Row>
        <div style={{ overflow: 'hidden' }}>
          <div style={{ float: 'right', marginBottom: 24 }}>
            <a style={{ marginLeft: 8 }} onClick={this.toggleAdvancedForm}>
              Simple
            </a>
          </div>
        </div>
      </Form>
    )
  }

  renderForm() {
    const { expandForm, advancedQueryForm } = this.state;
    const simpleForm = expandForm ? this.renderExpandedSimpleForm() : this.renderSimpleForm();
    return advancedQueryForm ? this.renderAdvancedQueryForm() : simpleForm;
  }

  render() {
    const {
      meteringPlaces,
      loading,
    } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, stepFormValues } = this.state;
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const menu = (
      <Menu>
        {/*<Menu.Item key="1"><Icon type="plus" />Add Sensor</Menu.Item>*/}
        {/*<Menu.Item key="2"><Icon type="upload" />Import Meter List</Menu.Item>*/}
        <Menu.Item key="3"><Icon type="download" />Export Metering Place List</Menu.Item>
      </Menu>
    );

    const actionList = (
      <Dropdown.Button
        overlay={menu}
        // disabled
        // style={{ marginLeft: 8 }}
      >
        <Icon type="plus"  /> Create
      </Dropdown.Button>
    );

    return (
      <PageHeaderWrapper title='Metering Places' content={this.renderForm()} action={actionList} count={meteringPlaces.count}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            {/*<div className={styles.tableListForm}>{this.renderForm()}</div>*/}
            <StandardTable
              selectedRows={selectedRows}
              loading={loading}
              dataSource={meteringPlaces.results}
              // rowKey='id'
              columns={this.columns}
              onChange={this.handleStandardTableChange}
              size='middle'
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        {stepFormValues && Object.keys(stepFormValues).length ? (
          <UpdateForm
            {...updateMethods}
            updateModalVisible={updateModalVisible}
            values={stepFormValues}
          />
        ) : null}
      </PageHeaderWrapper>
    );
  }
}

export default TableList;
