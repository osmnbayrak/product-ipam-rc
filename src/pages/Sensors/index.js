import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  Modal,
  message,
  Divider, Icon, Menu, Dropdown
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './Sensors.less';
import { bool } from 'prop-types';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title={<FormattedMessage id="title.place.floors.create" defaultMessage="Create Floor" />}
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.name" defaultMessage="Floor Name" />}>
        {form.getFieldDecorator('name', {
          rules: [{ required: true, message: 'Bu alan zorunludur.' }],
        })(<Input placeholder={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Floor Name" })} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.level" defaultMessage="Floor Level" />}>
        {form.getFieldDecorator('level', {
          rules: [{ required: true, message: 'Bu alan zorunludur.' }],
        })(<Input placeholder={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Floor Level" })} />)}
      </FormItem>
    </Modal>
  );
});

@Form.create()
class UpdateForm extends PureComponent {
  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  render() {
    const {
      form,
      handleUpdate,
      values
    } = this.props;

    const { updateModalVisible, handleUpdateModalVisible } = this.props;
    const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(values.id, fieldsValue);
      });
    };
    return (
      <Modal
        width={640}
        bodyStyle={{ padding: '32px 40px 48px' }}
        destroyOnClose
        title={formatMessage({ id: 'title.update', defaultMessage:"Update Form" })}
        style={{ width: '100%' }}
        visible={updateModalVisible}
        onCancel={() => handleUpdateModalVisible()}
        onOk={okHandle}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.name" defaultMessage="Floor Name" />}>
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: 'Bu alan zorunludur.' }],
            initialValue: values.attributes.name
          })(<Input placeholder={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Floor Name" })} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={<FormattedMessage id="title.place.floors.level" defaultMessage="Floor Level" />}>
          {form.getFieldDecorator('level', {
            rules: [{ required: true, message: 'Bu alan zorunludur.' }],
            initialValue: values.attributes.level
          })(<Input placeholder={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Floor Level" })} />)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ sensors, loading }) => ({
  sensors,
  loading: loading.models.sensors,
}))
@Form.create()
class TableList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    pagination: {
      page_size: 10,
      page: 1
    }
  };

  columns = [
    {
      title: <FormattedMessage id="vrf" defaultMessage="VRF" />,
      dataIndex: 'name',
    },
    {
      title: <FormattedMessage id="vrf_group" defaultMessage="VRF Group" />,
      dataIndex: 'group.name',
    },
    {
      title: <FormattedMessage id="ipv4" defaultMessage="Ipv4" />,
      render: (text, record) => (
        this.renderIpv(record.ipv4)
      )
    },
    {
      title: <FormattedMessage id="ipv6" defaultMessage="Ipv6" />,
      render: (text, record) => (
        this.renderIpv(record['ipv6'])
      )
    },
    {
      title: <FormattedMessage id="desc" defaultMessage="Description" />,
      dataIndex: 'desc',
    },
    {
      title: <FormattedMessage id="tenant" defaultMessage="Tenant" />,
      dataIndex: 'tenant'
    },
  ];

  renderIpv(boolean) {
    switch(boolean) {
      case true:
        return <span><Icon type='check'/></span>;
      case false:
        return <span><Icon type='cross'/></span>;
    }
  }

  componentDidMount() {
    const { dispatch } = this.props;

    dispatch({
      type: 'sensors/fetch',
      payload: this.state.pagination,
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'sensors/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'sensors/fetch',
      payload: {},
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'sensors/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      stepFormValues: record || {},
    });
  };

  handleRemove = (record) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'sensors/delete',
      payload: {
        id: record.id,
      },
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    dispatch({
      type: 'sensors/create',
      payload: {
        name: fields.name,
        level: fields.level,
      },
    }).then(() => {
      dispatch({
        type: 'sensors/fetch',
      });
      this.handleModalVisible();
    }).catch(() => {
      dispatch({
        type: 'sensors/fetch',
      });
    });

    // this.handleModalVisible();
  };

  handleUpdate = (id, fields) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'sensors/update',
      payload: {
        id,
        name: fields.name,
        level: fields.level,
      },
    });

    message.success('Updated');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Search" })}>
              {getFieldDecorator('name__icontains')(<Input placeholder={formatMessage({ id: 'title.place.floors.name', defaultMessage:"Search" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Another filter" })}>
              {getFieldDecorator('level')(<Input placeholder={formatMessage({ id: 'title.place.floors.level', defaultMessage:"Another filter" })} />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                <FormattedMessage id="btn.submit" defaultMessage="Submit" />
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                <FormattedMessage id="btn.clear" defaultMessage="Clear" />
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    const {
      sensors,
      loading,
    } = this.props;
    sensors.results = [
      {name: 'Vrf1', id:'1',  group: {name: 'vrf group1'}, tenant: 5, ipv4: true, ipv6: false, desc: 'Test desc of this vrf'},
      {name: 'Vrf2', id:'2', group: {name: 'vrf group2'}, tenant: 3, ipv4: false, ipv6: true, desc: 'Description test'},
      {name: 'Vrf3', id:'3', group: {name: 'vrf group3'}, tenant: 22, ipv4: false, ipv6: false, desc: 'Desc test'},
      {name: 'Vrf4', id:'4', group: {name: 'vrf group4'}, tenant: 52, ipv4: true, ipv6: true, desc: 'Test of this one'},
      {name: 'Vrf5', id:'5', group: {name: 'vrf group5'}, tenant: 12, ipv4: true, ipv6: true, desc: 'Descri of this vrf'},
      {name: 'Vrf6', id:'6', group: {name: 'vrf group6'}, tenant: 7, ipv4: true, ipv6: false, desc: 'Test desc of this vrf'},
      {name: 'Vrf7', id:'7', group: {name: 'vrf group7'}, tenant: 39, ipv4: false, ipv6: false, desc: 'Test desc of this vrf'},
    ]
    const { selectedRows, modalVisible, updateModalVisible, stepFormValues } = this.state;
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const menu = (
      <Menu>
        {/*<Menu.Item key="1"><Icon type="plus" />Add Sensor</Menu.Item>*/}
        <Menu.Item key="2"><Icon type="upload" />Import VRF table</Menu.Item>
        <Menu.Item key="3"><Icon type="download" />Export VRF table</Menu.Item>
      </Menu>
    );

    const actionList = (
      <Dropdown.Button
        overlay={menu}
        // disabled
        // style={{ marginLeft: 8 }}
        onClick={ () => router.push('/vrf/add') }
      >
        <Icon type="plus"  /> Create
      </Dropdown.Button>
    );

    return (
      <PageHeaderWrapper title='VRF Table' action={actionList} count={sensors.count}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <StandardTable
              selectedRows={selectedRows}
              loading={false}
              dataSource={sensors.results}
              // rowKey='id'
              columns={this.columns}
              onChange={this.handleStandardTableChange}
              size='middle'
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        {stepFormValues && Object.keys(stepFormValues).length ? (
          <UpdateForm
            {...updateMethods}
            updateModalVisible={updateModalVisible}
            values={stepFormValues}
          />
        ) : null}
      </PageHeaderWrapper>
    );
  }
}

export default TableList;
