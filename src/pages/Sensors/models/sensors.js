import {
  getSensors,
  createSensor,
  updateSensor,
  deleteSensor
} from '@/services/api';

export default {
  namespace: 'sensors',

  state: {
    results: []
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(getSensors, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *create({ payload }, { call }) {
      yield call(createSensor, payload);
    },
    *update({ payload }, { call }) {
      yield call(updateSensor, payload);
    },
    *delete({ payload }, { call }) {
      yield call(deleteSensor, payload);
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
