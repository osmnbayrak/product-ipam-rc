import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import router from 'umi/router';
import {Row,Col,Card,Form,Input,Button,Modal,message,Divider, Icon, Menu, Dropdown} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Highlighter from 'react-highlight-words';

import styles from './Planning.less';
import { bool } from 'prop-types';

@connect(({ allPlannings, loading }) => ({
    allPlannings,
    loading: loading.models.allPlannings,
}))

@Form.create()
class AllPlannings extends PureComponent {
  state = {
    searchText: '',
    modalVisible: false,
    updateModalVisible: false,
    filters: {},
    pagination: {
      page_size: 10,
      page: 1,
    },
    data: [],
    columns: [
        {title: 'Planning Name',
        render: (text, record) => (
            <a onClick={ () => router.push('/planningData/') }>{record.slug}</a>
        )},
        {title: 'id',
        dataIndex: 'id'
        }
    ]
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: () => (
      <div className="custom-filter-dropdown">
        <Input
          ref={node => { this.searchInput = node; }}
          placeholder={`Search ${dataIndex}`}
          value = {this.state.filters[dataIndex]}
          onChange = {(e) => this.setState({ filters: Object.assign({}, this.state.filters, {
            [dataIndex]: e.target.value,
          })})}
          onPressEnter={() => this.handleSearch(this.state.filters)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(this.state.filters)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(dataIndex)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
  })

  componentDidMount() {
    const { dispatch } = this.props;

    this.handleStandardTableChange({current: this.state.pagination.page, pageSize: this.state.pagination.page_size}, false)
  }

  handleStandardTableChange = (pagination, sorter) => {
    const { dispatch } = this.props;
    const scope = this

    Object.keys(scope.state.filters).map(function(keyName, keyIndex) {
      if (scope.state.filters[keyName] == '') {
        scope.state.filters[keyName] = undefined
      }
    })

    const params = {
      page: pagination.current,
      page_size: pagination.pageSize,
      ...scope.state.filters,
    };
    if (sorter.field) {
      params.ordering = `${sorter.order == 'ascend' ? '' : '-'}${sorter.field}`;
    }

    dispatch({
      type: 'allPlannings/fetch',
      payload: params,
    });
  };

  handleSearch = e => {
    const { dispatch } = this.props;

    this.handleStandardTableChange({current: this.state.pagination.page, pageSize: this.state.pagination.page_size}, false)
  };

  
  handleReset  = (e) => {
    const { dispatch } = this.props;
    this.setState({ filters: Object.assign({}, this.state.filters, {
      [e]: '',
    })})
    dispatch({
      type: 'allPlannings/fetch',
      payload: this.state.pagination,
    });
  };




  render() {
    const {
      allPlannings: { data },
      loading,
    } = this.props;
    console.log(data)

    const { modalVisible, updateModalVisible } = this.state;

    const menu = (
      <Menu>
        {/*<Menu.Item key="1"><Icon type="plus" />Add Sensor</Menu.Item>*/}
        <Menu.Item key="2"><Icon type="upload" />Import Plannings</Menu.Item>
        <Menu.Item key="3"><Icon type="download" />Export Plannings</Menu.Item>
      </Menu>
    );

    const actionList = (
      <Dropdown.Button
        overlay={menu}
        onClick={ () => router.push('/planning/add') }
      >
        <Icon type="plus"  /> Add New Planning
      </Dropdown.Button>
    );

    return (
    <PageHeaderWrapper title={<FormattedMessage id='planning.header'/>} action={actionList} count={10}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <StandardTable
              selectedRows={[]}
              loading={false}
              totalData={2}
              dataSource={data}
              rowKey='id'
              columns={this.state.columns}
              onChange={this.handleStandardTableChange}
              size='middle'
            />
          </div>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default AllPlannings;
