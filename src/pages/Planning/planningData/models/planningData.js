import {
    planningData,
  } from '@/services/api';
  
  export default {
    namespace: 'planningData',
  
    state: {
      data: [],
    },
  
    effects: {
      *fetch({ payload }, { call, put }) {
        const response = yield call(planningData, payload);
        yield put({
          type: 'save',
          payload: response,
        });
      },
      *create({ payload }, { call }) {
        yield call(createSensor, payload);
      },
      *update({ payload }, { call }) {
        yield call(updateSensor, payload);
      },
      *delete({ payload }, { call }) {
        yield call(deleteSensor, payload);
      },
    },
  
    reducers: {
      save(state, action) {
        return {
          ...state,
          data: action.payload,
        };
      },
    },
  };