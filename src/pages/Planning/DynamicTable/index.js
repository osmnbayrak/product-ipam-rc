import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import router from 'umi/router';
import {Row,Col,Card,Form,Input,Button,Modal,message,Divider, Icon, Menu, Dropdown} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Highlighter from 'react-highlight-words';

import styles from './Planning.less';
import { bool } from 'prop-types';

const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ planning, loading }) => ({
  planning,
  loading: loading.models.planning,
}))

@Form.create()
class TableList extends PureComponent {
  state = {
    searchText: '',
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    filters: {},
    stepFormValues: {},
    pagination: {
      page_size: 10,
      page: 1,
    },
    meta: {
      id: {
        columnName: 'ID',
        show: true,
        dataType: 'number',
        objField: null,
        order: true,
        search: true
      },
      logo: {
        columnName: 'Logo',
        show: true,
        dataType: 'string',
        objField: null,
        order: true,
        search: true
      },
      name: {
        columnName: 'Name',
        show: true,
        dataType: 'string',
        objField: null,
        order: true,
        search: true
      },
      description: {
        columnName: 'Description',
        show: true,
        dataType: 'string',
        objField: null,
        order: true,
        search: false
      },
    },
    data: [],
    columns: []
  };

  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: () => (
      <div className="custom-filter-dropdown">
        <Input
          ref={node => { this.searchInput = node; }}
          placeholder={`Search ${dataIndex}`}
          value = {this.state.filters[dataIndex]}
          onChange = {(e) => this.setState({ filters: Object.assign({}, this.state.filters, {
            [dataIndex]: e.target.value,
          })})}
          onPressEnter={() => this.handleSearch(this.state.filters)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(this.state.filters)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(dataIndex)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
  })

  createColumns = () => {
    const scope = this
    const meta = this.state.meta
    const cols = []
    Object.keys(meta).map(function(keyName, keyIndex) {
      if (meta[keyName].dataType == 'string' && meta[keyName].show) {
        cols.push(
          {title: <FormattedMessage id={meta[keyName].columnName} defaultMessage={meta[keyName].columnName} />, 
          dataIndex: keyName,
          key: keyName,
          sorter: true,
          ...scope.getColumnSearchProps(keyName),
        },
          )
      } else if (meta[keyName].dataType == 'boolean' && meta[keyName].show) {
        cols.push(
          {title: <FormattedMessage id={meta[keyName].columnName} defaultMessage={meta[keyName].columnName} />,
            render: (text, record) => (
              record[keyName] ? <span>Tik(true)</span> : <span>Çarpi(false)</span>
            ),
            sorter: true,
            key: keyName.toString(),
            ...scope.getColumnSearchProps(keyName),
            },
          )
      } else if (meta[keyName].dataType == 'number' && meta[keyName].show) {
        cols.push(
          {title: <FormattedMessage id={meta[keyName].columnName} defaultMessage={meta[keyName].columnName} />,
            render: (text, record) => (
              <span>{record[keyName]}</span>
            ),
            sorter: true,
            key: keyName.toString(),
            ...scope.getColumnSearchProps(keyName),
            },
          )
      } else if (meta[keyName].dataType == 'object' && meta[keyName].show) {
        cols.push(
          {title: <FormattedMessage id={meta[keyName].columnName} defaultMessage={meta[keyName].columnName} />,
          dataIndex: `${keyName}.${meta[keyName].objField}`,
          sorter: true,
          ...scope.getColumnSearchProps(keyName),
          }
          )
      }
        
      })
    this.setState({columns: cols})
  }

  componentDidMount() {
    this.createColumns()
    const { dispatch } = this.props;

    this.handleStandardTableChange({current: this.state.pagination.page, pageSize: this.state.pagination.page_size}, false)
  }

  handleStandardTableChange = (pagination, sorter) => {
    const { dispatch } = this.props;
    const scope = this

    Object.keys(scope.state.filters).map(function(keyName, keyIndex) {
      if (scope.state.filters[keyName] == '') {
        scope.state.filters[keyName] = undefined
      }
    })

    const params = {
      page: pagination.current,
      page_size: pagination.pageSize,
      ...scope.state.filters,
    };
    if (sorter.field) {
      params.ordering = `${sorter.order == 'ascend' ? '' : '-'}${sorter.field}`;
    }

    dispatch({
      type: 'planning/fetch',
      payload: params,
    });
  };

  handleSearch = e => {
    const { dispatch } = this.props;

    this.handleStandardTableChange({current: this.state.pagination.page, pageSize: this.state.pagination.page_size}, false)
  };

  
  handleReset  = (e) => {
    const { dispatch } = this.props;
    this.setState({ filters: Object.assign({}, this.state.filters, {
      [e]: '',
    })})
    dispatch({
      type: 'planning/fetch',
      payload: this.state.pagination,
    });
  };




  render() {
    const {
      planning: { data },
      loading,
    } = this.props;


    const { selectedRows, modalVisible, updateModalVisible, stepFormValues } = this.state;

    const menu = (
      <Menu>
        {/*<Menu.Item key="1"><Icon type="plus" />Add Sensor</Menu.Item>*/}
        <Menu.Item key="2"><Icon type="upload" />Import VRF table</Menu.Item>
        <Menu.Item key="3"><Icon type="download" />Export VRF table</Menu.Item>
      </Menu>
    );

    const actionList = (
      <Dropdown.Button
        overlay={menu}
        // disabled
        // style={{ marginLeft: 8 }}
        onClick={ () => router.push('/vrf/add') }
      >
        <Icon type="plus"  /> Create
      </Dropdown.Button>
    );

    return (
    <PageHeaderWrapper title={<FormattedMessage id='planning.header'/>} action={actionList} count={data.count}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <StandardTable
              selectedRows={selectedRows}
              loading={false}
              totalData={data.count}
              dataSource={data.results}
              rowKey='id'
              columns={this.state.columns}
              onChange={this.handleStandardTableChange}
              size='middle'
            />
          </div>
        </Card>
        {stepFormValues && Object.keys(stepFormValues).length ? (
          <UpdateForm
            {...updateMethods}
            updateModalVisible={updateModalVisible}
            values={stepFormValues}
          />
        ) : null}
      </PageHeaderWrapper>
    );
  }
}

export default TableList;
